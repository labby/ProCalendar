<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


$files_to_register = array(
	'add.php',
	'delete.php',
	'functions.php',
	'modify.php',
	'modify_customs.php',
	'modify_layout.php',
	'modify_settings.php',
	'languages/support-DE.php',
	'languages/support-NL.php',
	'languages/support-EN.php',
	'save.php',
	'save_customs.php',
	'save_layout.php',
	'save_settings.php'	
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );
