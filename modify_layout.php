<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

$database = LEPTON_database::getInstance();

$MOD_PROCALENDAR = procalendar::getInstance()->language;

$db = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_procalendar_settings WHERE section_id = ".$section_id ,
	true,
	$db,
	false
);

$header     = $db["header"];
$footer     = $db["footer"];
$posttempl  = $db["posttempl"];

// Set raw html <'s and >'s to be replace by friendly html code
$raw = ['<', '>'];
$friendly = ['&lt;', '&gt;'];

?>
<h2><?php echo $TEXT['TEMPLATE']; ?></h2>
<form name="modify_startday" method="post" action="<?php echo LEPTON_URL; ?>/modules/procalendar/save_layout.php">
  <input type="hidden" name="page_id" value="<?php echo $page_id; ?>">
  <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
  <table cellpadding="2" cellspacing="1" border="0" width="98%" class="customfields">
    <tr>
      <td valign="top"><?php echo $TEXT['HEADER']; ?>:</td>
      <td colspan="2">
        <textarea name="header"><?php echo str_replace($raw, $friendly, $header); ?></textarea>
      </td> 
    </tr>
     <tr>
      <td valign="top"><?php echo $TEXT['FOOTER']; ?>:</td>
      <td colspan="2">
        <textarea name="footer"><?php echo str_replace($raw, $friendly, $footer); ?></textarea>
      </td>
    </tr>    
	<tr>
    <tr>
      <td valign="top"><?php echo $TEXT['POST']; ?>:</td>
      <td colspan="2">
        <textarea name="posttempl" style="height:350px;"><?php echo str_replace($raw, $friendly, $posttempl); ?></textarea>
      </td>
    </tr>

      <td align="right" colspan="3"><input class="ui button edit_button" type="submit" value="<?php echo $MOD_PROCALENDAR['SAVE']; ?>"></td>
    </tr>
  </table>
</form>

<br>
<input type="button" class="ui button edit_button" value="<?php echo $MOD_PROCALENDAR['BACK']; ?>" onclick="javascript: window.location = '<?php echo LEPTON_URL."/modules/procalendar/modify_settings.php?page_id=$page_id&amp;section_id=$section_id"; ?>';" />
<?php

$admin->print_footer();

