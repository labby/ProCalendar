<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php 

$mod_headers = [
    'backend' => [
        'js' => [
            'modules/procalendar/js/picker/spectrum.js',
            'modules/procalendar/js/date.js',
            'modules/lib_jquery/jquery-ui/jquery-ui.min.js',
            'modules/lib_fomantic/dist/semantic.min.js'
        ],
        'css' => [
            [
                'media' => 'all',
                'file' => 'modules/lib_jquery/jquery-ui/jquery-ui.min.css'
            ],
            [
                'media' => 'all',
                'file' => 'modules/procalendar/js/picker/spectrum.css'
            ],
            [
                'media' => 'all',
                'file' => 'modules/lib_fomantic/dist/semantic.min.css'
            ]		   
        ]
    ]
];

/**
 *	This one is a little bit specific: we are looking for a
 *	matching language-file inside the lib_jquery module.
 *
 */
$look_for = '/modules/lib_jquery/jquery-ui/i18n/datepicker-'.strtolower(LANGUAGE).'.js';
if (file_exists(LEPTON_PATH.$look_for))
{
    $mod_headers['backend']['js'][] = $look_for;
}
