/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */
 
function procalendar_change_cat( aBaseURL, aPageID, aSectionID, aLeptokenHash, aGroupID ) {
	var s = aBaseURL+"?page_id="+aPageID;
	s += "&section_id="+aSectionID;
	s += "&group_id="+aGroupID;
	s += "&leptoken="+aLeptokenHash;
	
	document.location.href= s;

}
