<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

global $monthnames,$weekdays,$public_stat;
$monthnames = [1=>"Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"];
$weekdays = [1=>"Ma", "Di", "Wo", "Do", "Vr", "Za", "Zo"];
$public_stat = ["Openbaar", "Priv&eacute;", "ingelogd"];

$MOD_PROCALENDAR = [
	'CAL-OPTIONS' => "Opties",
	'CAL-OPTIONS-STARTDAY' => "Weekstart",
	'CAL-OPTIONS-STARTDAY-1' => "Maandag",
	'CAL-OPTIONS-STARTDAY-2' => "Zondag",
	'CAL-OPTIONS-USETIME' => "Tijden",
	'CAL-OPTIONS-USETIME-1' => "Gebruik geen tijden",
	'CAL-OPTIONS-USETIME-2' => "Gebruik wel tijden",
	'CAL-OPTIONS-FORMAT' => "Datumweergave",
	'CAL-OPTIONS-ONEDATE' => "Datum",
	'CAL-OPTIONS-ONEDATE-1' => "Gebruik alleen begindatum",
	'CAL-OPTIONS-ONEDATE-2' => "Gebruik begindatum en einddatum",
	'CATEGORY-MANAGEMENT' => "Categoriebeheer",
	'CATEGORY' => "Categorie",
	'CHOOSE-CATEGORY' => "Categorie...",
	'ACTIVE' => "Actief",
	'BACK' => "&laquo, Terug",
	'DATE-AND-TIME' => "Datum en tijd",
	'NOTIME' => "Tijd onbekend",
	'NODATES' => "Geen activiteiten...",
	'NODETAILS' => "Geen details beschikbaar...",
	'TIMESTR' => "uur",
	'DEADLINE' => "Einde",
	'DELETE' => "Verwijderen",
	'DESCRIPTION' => "Beschrijving",
	'NO_DESCRIPTION' => "Geen beschrijving beschikbaar...",
	'FROM' => "Start",
	'NEW-EVENT' => "Nieuw",
	'NEW' => "Nieuw",
	'NON-SPECIFIED' => "n.v.t.",
	'PLACE' => "Plaats", 
	'SAVE' => "Opslaan",
	'NAME' => "Naam",
	'VISIBLE' => "Zichtbaarheid",
	'SAVE-AS-NEW' => "Opslaan als nieuw",
	'SETTINGS' => "Instellingen",
	'ADVANCED-SETTINGS' => "Geavanceerde instellingen",
	'TO' => "Eind",
	'USE-THIS' => "Activeren",
	'CALENDAR-DEFAULT-TEXT' => "", //mogelijkheid voor standaardtitel leegmaken
	'CALENDAR-BACK-MONTH' => "Dagoverzicht",  
	'DATE' => "Datum",
	'CUSTOMS' => "Extra velden",
	'CUSTOM' => "Extra veld",
	'CUSTOM_TEMPLATE' => "Veld-template",
	'USE_CUSTOM' => "Type",
	'CUSTOM_NUMBER' => "Veldnummer",
	'CUSTOM_TYPE' => "Veldtype",
	'CUSTOM_NAME' => "Veldnaam",
	'CUSTOM_OPTIONS-0' => "Ongebruikt",
	'CUSTOM_OPTIONS-1' => "Tekstveld",
	'CUSTOM_OPTIONS-2' => "Tekstvak",
	'CUSTOM_OPTIONS-3' => "link",
	'CUSTOM_OPTIONS-4' => "Afbeelding",
	'CUSTOM_SELECT_IMG' => "Selecteer afbeelding",
	'CUSTOM_CHOOSE_IMG' => "Geen afbeelding",
	'CUSTOM_SELECT_LEPTONLINK' => "Selecteer pagina",
	'RESIZE_IMAGES' => "Afbeeldingen verkleinen",
	'SUPPORT_INFO' => "Hulpinformatie",
	'SUPPORT_INFO_INTRO' => "Voordat u deze module gebruikt, lees a.u.b. eerst de ",
	'FORMAT_ACTION' => "De kleur kan aangepast worden voor elke Categorie",
	'FORMAT_DAY' => "Deze kleur in de Kalender gebruiken?",
	'MAKE_REC' => "Herhalingen?",
	'DAILY' => "dagelijks",
	'WEEKLY' => "wekelijks",
	'MONTHLY' => "maandelijks",
	'YEARLY' => "jaarlijks",
	'DAYS' => "dagen",
	'DAY' => "dag(en)",
	'EVERY' => "elke",
	'EVERY_SINGLE' => "elke",
	'WEEK_ON' => "we(e)k(en) op",
	'OF_EVERY' => "van elke",
	'OF_MONATS' =>"maand",
	'IN' =>"in",
	'COUNT' => array( 1=>"eerste", "tweede", "derde", "vierde", "laatste"),
	'NOT_AT' =>"Niet op",
	'USE_EXCEPTION' => "Uitzonderingen?",
	'DATES' => "datums",
	'NEVER' => "nooit",
	'ISREC_MESSAGE' => "Deze datum is deel van een reeks: --OK-- voor aanpassen van de hele reeks. -- Cancel-- voor overschrijven of een nieuwe.",
	'REC_OVERWRITE_MESSAGE' => "Deze datum is deel van een reeks. Hierdoor wordt de oorspronkelijke reeks weer geldig.",
	'REC_OVERWRITE' => "Overschrijven",
];
