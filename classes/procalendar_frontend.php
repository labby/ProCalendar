<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


class procalendar_frontend extends LEPTON_abstract_frontend
{
	
	public string $fe_action = '';
	public array $all_entries = [];

	public LEPTON_database $database;
	static $instance;
	
	public function initialize () {
		
		$this->database = LEPTON_database::getInstance();
		$this->init_addon();		
		$this->fe_action = LEPTON_URL.PAGES_DIRECTORY.LEPTON_frontend::getInstance()->page['link'].PAGE_EXTENSION;	
	
	}	

	public function init_addon( $iPageID = 0, $iSectionID = 0 ) 
	{
		// 1.0  Get all entries
		
		// 1.1  Make sure that the "result" of the query is store in an empty array!
		$this->all_entries = [];
		
		// 1.2  "Ask" for entries 
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_procalendar_actions WHERE section_id = ".$iSectionID ,
			true,
			$this->all_entries,
			true
		);
		
		// 2.0  Update the 'fe_action' property of this class 
		//      Please keep in mind that this can be call by an droplet! 
		$this->fe_action = LEPTON_URL.PAGES_DIRECTORY.LEPTON_frontend::getInstance()->page['link'].PAGE_EXTENSION;	
	}
}
