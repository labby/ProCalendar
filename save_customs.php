<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


// Include admin wrapper script
require LEPTON_PATH.'/modules/admin.php';

$page_id      = $admin->getValue('page_id');
$section_id   = $admin->getValue('section_id');

$baseTerms = ['usecustom', 'custom', 'customtemplate'];

for ($i = 1; $i < 10; $i++)
{
    foreach ($baseTerms as $term)
    {
        $termName = $term.$i; 
        $$termName = $admin->getValue($termName, 'string');
    }
}

$resize = $admin->getValue('resize', 'string');

$sql = "UPDATE ";
$sql .= TABLE_PREFIX."mod_procalendar_settings SET "; // create rest of the sql-query
$sql .= "usecustom1='$usecustom1', ";
$sql .= "customtemplate1='$customtemplate1', ";
$sql .= "custom1='$custom1', ";
$sql .= "usecustom2='$usecustom2', ";
$sql .= "customtemplate2='$customtemplate2', ";
$sql .= "custom2='$custom2', ";
$sql .= "usecustom3='$usecustom3', ";
$sql .= "customtemplate3='$customtemplate3', ";
$sql .= "custom3='$custom3', ";
$sql .= "usecustom4='$usecustom4', ";
$sql .= "customtemplate4='$customtemplate4', ";
$sql .= "custom4='$custom4', ";
$sql .= "usecustom5='$usecustom5', ";
$sql .= "customtemplate5='$customtemplate5', ";
$sql .= "custom5='$custom5', ";
$sql .= "usecustom6='$usecustom6', ";
$sql .= "customtemplate6='$customtemplate6', ";
$sql .= "custom6='$custom6', ";
$sql .= "usecustom7='$usecustom7', ";
$sql .= "customtemplate7='$customtemplate7', ";
$sql .= "custom7='$custom7', ";
$sql .= "usecustom8='$usecustom8', ";
$sql .= "customtemplate8='$customtemplate8', ";
$sql .= "custom8='$custom8', ";
$sql .= "usecustom9='$usecustom9', ";
$sql .= "customtemplate9='$customtemplate9', ";
$sql .= "custom9='$custom9', "; 
$sql .= "resize='$resize' ";        
$sql .= " WHERE section_id=$section_id";
     
$database->query($sql);

if($database->is_error()) {
  $admin->print_error($database->get_error(), LEPTON_URL."/modules/procalendar/modify_settings.php?page_id=$page_id&section_id=$section_id");
} else {
  $admin->print_success($TEXT['SUCCESS'], LEPTON_URL."/modules/procalendar/modify_settings.php?page_id=$page_id&section_id=$section_id");
}

$admin->print_footer();
