<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php



$table_fields="
	`section_id` int(11) NOT NULL DEFAULT '0',
	`page_id` int(11) NOT NULL DEFAULT '0',
	`settings` text,
	`startday` int(11) NOT NULL DEFAULT '0',
	`onedate` int(11) NOT NULL DEFAULT '0',
	`usetime` int(11) NOT NULL DEFAULT '0',
	`useformat` varchar(15) NOT NULL DEFAULT '0',
	`useifformat` varchar(15) NOT NULL DEFAULT '0',
	`usecustom1` int(11) NOT NULL DEFAULT '0',
	`custom1` text,
	`customtemplate1` text,
	`usecustom2` int(11) NOT NULL DEFAULT '0',
	`custom2` text,
	`customtemplate2` text,
	`usecustom3` int(11) NOT NULL DEFAULT '0',
	`custom3` text,
	`customtemplate3` text,
	`usecustom4` int(11) NOT NULL DEFAULT '0',
	`custom4` text,
	`customtemplate4` text,
	`usecustom5` int(11) NOT NULL DEFAULT '0',
	`custom5` text,
	`customtemplate5` text,
	`usecustom6` int(11) NOT NULL DEFAULT '0',
	`custom6` text,
	`customtemplate6` text,
	`usecustom7` int(11) NOT NULL DEFAULT '0',
	`custom7` text,
	`customtemplate7` text,
	`usecustom8` int(11) NOT NULL DEFAULT '0',
	`custom8` text,
	`customtemplate8` text,
	`usecustom9` int(11) NOT NULL DEFAULT '0',
	`custom9` text,
	`customtemplate9` text,
	`resize` int(11) NOT NULL DEFAULT '0',
	`header` text,
	`footer` text,
	`posttempl` text,
	PRIMARY KEY (section_id)
";
LEPTON_handle::install_table("mod_procalendar_settings", $table_fields);

  
$table_fields="
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`section_id` int(11) NOT NULL DEFAULT '0',
	`page_id` int(11) NOT NULL DEFAULT '0',
	`owner` int(11) NOT NULL DEFAULT '0',
	`date_start` date NOT NULL DEFAULT '1980-01-01',
	`time_start` time NOT NULL DEFAULT '00:00:00',
	`date_end` date NOT NULL DEFAULT '1980-01-01',
	`time_end` time NOT NULL DEFAULT '00:00:00',
	`acttype` int(4) NOT NULL DEFAULT '0',
	`name` varchar(255) NOT NULL DEFAULT '',
	`description` text,
	`custom1` text,
	`custom2` text,
	`custom3` text,
	`custom4` text,
	`custom5` text,
	`custom6` text,
	`custom7` text,
	`custom8` text,
	`custom9` text,
	`public_stat` int(4) NOT NULL DEFAULT 0,
	`rec_id` int(11) NOT NULL DEFAULT 0,
	`rec_day` varchar(255) NOT NULL DEFAULT '',
	`rec_week` varchar(255) NOT NULL DEFAULT '',
	`rec_month` varchar(255) NOT NULL DEFAULT '',
	`rec_year` varchar(255) NOT NULL DEFAULT '',
	`rec_count` int(6) NOT NULL DEFAULT 0,
	`rec_exclude` varchar(255) NOT NULL DEFAULT '',
	PRIMARY KEY (id)
";
LEPTON_handle::install_table("mod_procalendar_actions", $table_fields);

$table_fields="
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`section_id` int(11) NOT NULL DEFAULT '0',
	`name` varchar(255) NOT NULL DEFAULT '',
	`format` varchar(255) NOT NULL DEFAULT '',
	`format_days` int(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (id)
";
LEPTON_handle::install_table("mod_procalendar_eventgroups", $table_fields);	 
 
// Insert info into the search table
// Module query info
$field_info = [];
$field_info['page_id'] = 'page_id';
$field_info['title'] = 'page_title';
$field_info['link'] = 'link';
$field_info['description'] = 'description';
$field_info['modified_when'] = 'modified_when';
$field_info['modified_by'] = 'modified_by';
$field_info = serialize($field_info);

// Query start
$query_start_code = "SELECT [TP]pages.page_id, [TP]pages.page_title,  [TP]pages.link, [TP]pages.description, [TP]pages.modified_when, [TP]pages.modified_by FROM [TP]mod_procalendar_actions, [TP]pages WHERE ";

// Query body
$query_body_code = " 
	[TP]pages.page_id    = [TP]mod_procalendar_actions.page_id AND [TP]mod_procalendar_actions.name LIKE \'%[STRING]%\'
	OR [TP]pages.page_id = [TP]mod_procalendar_actions.page_id AND [TP]mod_procalendar_actions.description LIKE \'%[STRING]%\'
";
	
$field_values="
	(NULL,'module', 'procalendar', '".$field_info."'),	
	(NULL,'query_start', '".$query_start_code."', 'procalendar'),
	(NULL,'query_body', '".$query_body_code."', 'procalendar'),
	(NULL,'query_end', '', 'procalendar')
";
LEPTON_handle::insert_values('search', $field_values);
 
  
// Make calendar images directory		
if(LEPTON_core::make_dir(LEPTON_PATH.MEDIA_DIRECTORY.'/calendar')) 
{
	// Add a index.php file to prevent directory
	$content = ''.
	"<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */

	header('Location: ../');
	?>";
	$handle = fopen(LEPTON_PATH.MEDIA_DIRECTORY.'/calendar/index.php', 'w');
	fwrite($handle, $content);
	fclose($handle);
	LEPTON_core::change_mode(LEPTON_PATH.MEDIA_DIRECTORY.'/calendar/index.php', 'file');			
}
