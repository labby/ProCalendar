<?php

/**
 *
 *	@module			ProCalendar
 *	@version		see info.php of this module
 *	@authors		David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@copyright		2012-2024 David Ilicz Klementa, Burkhard Hekers, Jurgen Nijhuis, John Maats,erpe
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *	Based on MyCalendar by Burkhard Hekers
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

//get instance of own module class
$oPC = procalendar::getInstance();
$oPC->init_section( $page_id, $section_id );

require_once __DIR__.'/functions.php' ;

$month=date("n");
$year=date("Y");
$day=date("j");
// $day = "";
if ((isset($_GET['day']))and($_GET['day']!="")) {
  $day = $_GET['day'];
}

if (isset($_GET['edit'])) {
  $editMode = $_GET['edit'];
  } else {
  $editMode = "no";
}
if (isset($_GET['dayview'])) {
  $dayview = (int)$_GET['dayview'];
} else {
  $dayview = (int)0;
}
if ((isset($_GET['month']))and($_GET['month']!="")) {
  $month = (int)$_GET['month'];
} 
if ((isset($_GET['year']))and((int)$_GET['year']!="-")) {
  $year = (int)$_GET['year'];
} 
if (isset($_GET['show'])) {
  $show = (int)$_GET['show'];
} else {
  $show = 0;
}
if (isset($_GET['id'])) {
  $edit_id = (int)$_GET['id'];
} else {
  $edit_id = 0;
}

$tm_zacatek = "$year-$month-1";
$tm_konec = "$year-$month-".DaysCount($month,$year);

$actions = fillActionArray($tm_zacatek, $tm_konec, $section_id);
if(!is_array($actions)) 
{
    $actions = [];
}
$action_types = fillActionTypes($section_id);

// For some php reason this must be here and not in the functions file where it was.
// If in functions the ckeditor will error with array_key_exists() expects parameter 2 to be array, null given in .../modules/ckeditor/include.php on line 182
// It seems like global doesn't work from a included function.

if(!isset($wysiwyg_editor_loaded)) 
{
    $wysiwyg_editor_loaded = true;

    if (!defined('WYSIWYG_EDITOR') OR WYSIWYG_EDITOR == "none" ) 
    {
        LEPTON_handle::register('show_wysiwyg_editor');
        // @DEPRECATED_TEMP 20240316 : obsolete with next release 7.2.0
        if (!function_exists("show_wysiwyg_editor"))
        {
            function show_wysiwyg_editor($name,$id,$content,$width,$height,$prompt) 
            {
                echo '<textarea name="'.$name.'" id="'.$id.'" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';
            }
        }
    }
    else 
    {
        require_once(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
    }
}
?>

<div class="modify_content">
<?php 
  ShowCalendar($month, $year, $actions,$section_id,true);
  ShowActionListEditor($actions, $day, $page_id, $dayview );
  ShowActionEditor($actions, $day, $show, $dayview, $editMode, $month, $year, $edit_id); 
 ?>
</div>
